package com.facebookclone.facebookclone.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.facebookclone.facebookclone.model.User;
import com.facebookclone.facebookclone.repository.UserRepository;

@CrossOrigin("http://localhost:4200/")
@RestController
public class UserController {
	
	@Autowired
	UserRepository userRepository;
	
	@GetMapping("/users")
	public List<User> getAllUser() {
		return userRepository.findAll();
	}

	@PostMapping("/users")
	public User userRegistration(@Valid @RequestBody User user) {
		return userRepository.save(user); 
	}
	
	@GetMapping("/login")
	public String welcome() {
		return "welcome to spacebook";
	}
}
