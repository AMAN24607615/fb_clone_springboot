package com.facebookclone.facebookclone;

import java.util.Arrays;
import java.util.Collections;
import java.util.Date;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;
import org.springframework.web.servlet.config.annotation.PathMatchConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;


@SpringBootApplication
public class FacebookcloneApplication {

	public static void main(String[] args) {

		SpringApplication.run(FacebookcloneApplication.class, args);
		System.out.println("Facebook Clone Project "+ new Date());
	}
	@Bean
	  
	public CorsFilter corsFilter() {
	      final UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
	      final CorsConfiguration config = new CorsConfiguration();
	      config.setAllowCredentials(true);
	      config.setAllowedOriginPatterns(Collections.singletonList("*"));
	      config.setAllowedHeaders(Collections.singletonList("*"));
	      //config.setAllowedHeaders(Arrays.asList("Origin", "Content-Type", "Accept","Authorization"));
	      config.setAllowedMethods(Arrays.asList("GET", "POST", "PUT", "OPTIONS", "DELETE", "PATCH"));
	      source.registerCorsConfiguration("/**", config);
	      return new CorsFilter(source);
	  }
	
	  
	 //The below code is added on 07-01-2022 to get file name with path
	  @Configuration
	    public static class PathMatchingConfigurationAdapter extends WebMvcConfigurerAdapter {
	 
	        @Override
	        public void configurePathMatch(PathMatchConfigurer configurer) {
	            configurer.setUseSuffixPatternMatch(false);
	        }
	    }

}
